import os

from telebot import TeleBot, types
from telebot.types import Message
from cat_api.catapi import CatApi
from command_handler import CommandHandler
from persistent_storage.tournaments_repository import TournamentsRepository
from persistent_storage.users_repository import UsersRepository
from states import State
import schedule.schedule_parser as schedule_parser

token = os.environ["BOT_TOKEN"]
bot = TeleBot(token)
schedule_path = "schedule\\schedule.txt"
events = schedule_parser.get_events_from_file(schedule_path)
tournaments = TournamentsRepository("persistent_storage\\storage\\tournaments")
users = UsersRepository("persistent_storage\\storage\\users")
handler = CommandHandler(bot, CatApi(), events, tournaments)

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message:
        bot.answer_callback_query(callback_query_id=call.id, show_alert=True, text="Товар добавлен в корзину")
        if call.data == "test3":
            file = open(str(call.message.chat.id) + '.txt', "a")
            file.write("Пицца Классик(20 см)" + '\n')
            file.close()
        if call.data == "test1":
            file = open(str(call.message.chat.id) + '.txt', "a")
            file.write("Пицца Классик(40 см)" + '\n')
            file.close()
        if call.data == "test2":
            file = open(str(call.message.chat.id) + '.txt', "a")
            file.write("Пицца Классик(30 см)" + '\n')
            file.close()

@bot.message_handler(commands=['start'])
def start_handler(message: Message):
    user_id = message.from_user.id
    if not users.exists(user_id):
        users.save(user_id, state=State.MAIN)
        handler.start(message)
        file = open(str(user_id) + '.txt', "w")
        file.write("Корзина:\n")
        file.close()
    else:
        help_handler(message)
        handler.keyboard_in_menu(message)

@bot.message_handler(commands=['help'])
def help_handler(message: Message):
    handler.help(message)

@bot.message_handler(commands=['tournaments'])
def send_list_of_tournaments(message: Message):
    user_id = message.from_user.id
    if not users.exists(user_id):
        help_handler(message)
        return
    state = State.TOURNAMENTS
    users.save(user_id, state=state)
    handler.keyboard_in_tournaments(message)

@bot.message_handler(func=lambda message: True, content_types=['text'])
def default_handler(message: Message):
    user_id = message.from_user.id
    if message.text == u'\U0001F697 Корзина':
        state = State.BAG
        users.save(user_id, state=state)
        handler.keyboard_in_bag(message)
        return
    if message.text == u'\U0001F697 Доставить':
        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        button_phone = types.KeyboardButton(text="Отправить номер телефона", request_contact=True)
        button_geo = types.KeyboardButton(text="Отправить местоположение", request_location=True)
        keyboard.add(button_phone, button_geo)
        bot.send_message(message.chat.id, "Отправь мне свой номер телефона", reply_markup=keyboard)
        return
    if message.text == u'\U0001F355 Пицца':
        state = State.PIZZA
        users.save(user_id, state=state)
        handler.keyboard_in_pizza(message)
        return
    if message.text == u'\U0001F355 Классик':
        state = State.PIZZA1
        users.save(user_id, state=state)
        handler.keyboard_in_pizza1(message)
        return
    if message.text == u'\U0001F355 Куриная':
        state = State.PIZZA2
        users.save(user_id, state=state)
        handler.keyboard_in_pizza2(message)
        return
    if message.text == u'\U0001F355 Мясная':
        state = State.PIZZA3
        users.save(user_id, state=state)
        handler.keyboard_in_pizza3(message)
        return
    if message.text == u'\U0001F355 Итальянская':
        state = State.PIZZA4
        users.save(user_id, state=state)
        handler.keyboard_in_pizza4(message)
        return
    if message.text == u'\U0001F355 Неаполитанская':
        state = State.PIZZA5
        users.save(user_id, state=state)
        handler.keyboard_in_pizza5(message)
        return
    if message.text == u'\U0001F355 Перейти в корзину':
        state = State.BAG
        users.save(user_id, state=state)
        handler.keyboard_in_bag(message)
        return

    if not users.exists(user_id):
        handler.bot.send_message(message.chat.id, "Используй /start чтобы начать общение с ботом.")
        return

    state = users.read(user_id)
    next_state = handler.handle_default(message, state)
    if next_state is None:
        pass
    else:
        users.save(user_id, next_state)

bot.polling()
