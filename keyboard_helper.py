from typing import List

from telebot import types

from Tournament.tournaments import ButtonType


def create_keyboard(row_width: int, buttons: List[ButtonType]) -> types.ReplyKeyboardMarkup:
    keyboard = types.ReplyKeyboardMarkup(row_width=row_width, resize_keyboard=True, one_time_keyboard=True)
    keyboard.add(*[types.KeyboardButton(str(button)) for button in buttons])
    return keyboard

def create_tournaments_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Soccer, ButtonType.Hat, ButtonType.Poker, ButtonType.Back]
    return create_keyboard(row_width=3, buttons=buttons)

def create_back_keyboard() -> types.ReplyKeyboardMarkup:
    return create_keyboard(row_width=3, buttons=[ButtonType.Back])

def create_menu_keyboard() -> types.ReplyKeyboardMarkup:
    #buttons = [ButtonType.Bag, ButtonType.Pizza, ButtonType.Snack, ButtonType.Dessert, ButtonType.Drink, ButtonType.Salad]
    buttons = [ButtonType.Pizza, ButtonType.Snack, ButtonType.Dessert, ButtonType.Drink,ButtonType.Salad]
    return create_keyboard(row_width=1, buttons=buttons)

def create_pizza_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Pizza1, ButtonType.Pizza2, ButtonType.Pizza3, ButtonType.Pizza4, ButtonType.Pizza5, ButtonType.Back]
    return create_keyboard(row_width=1, buttons=buttons)

def create_pizza1_keyboard() -> types.ReplyKeyboardMarkup:
    #buttons = [ButtonType.Pizza_info_no, ButtonType.Pizza_info_yes]
    buttons = [ButtonType.Pizza_info_no]
    return create_keyboard(row_width=1, buttons=buttons)

def create_pizza_menu_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Pizza_menu_info_no, ButtonType.Pizza_bag]
    return create_keyboard(row_width=2, buttons=buttons)

def create_order_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Pizza_back_menu, ButtonType.Order_i, ButtonType.Order_restaurant]
    return create_keyboard(row_width=3, buttons=buttons)

def create_bag() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Pizza_back_menu, ButtonType.Pizza_order, ButtonType.Delete_Bag]
    return create_keyboard(row_width=2, buttons=buttons)
