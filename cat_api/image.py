class Image:
    """Data transfer object for image meta"""

    def __init__(self, id: str, url: str, source_url: str):
        self.id = id
        self.url = url
        self.source_url = source_url

    def __eq__(self, other: 'Image'):
        if self.id != other.id:
            return False
        if self.url != other.url:
            return False
        return self.source_url == other.source_url

    def __str__(self):
        return "Image(id='{}', url='{}', source_url='{}')".format(self.id, self.url, self.source_url)
