import pytest

import cat_api.cat_api_xml_parser
import cat_api.cat_api_request_performer
import cat_api.catapi as api
import cat_api.image


@pytest.fixture
def xml_source():
    return """<?xml version="1.0"?>
<response>
  <data>
    <images>
      <image>
        <url>url1</url>
        <id>id1</id>
        <source_url>source_url1</source_url>
      </image>
      <image>
        <url>url2</url>
        <id>id2</id>
        <source_url>source_url2</source_url>
      </image>
    </images>
  </data>
</response>"""

@pytest.fixture
def bad_xml_source():
    return """<?xml version="1.0"?>
<response>
  <data>
    <pics>
      <image>
        <url>url1</url>
        <id>id1</id>
        <source_url>source_url1</source_url>
      </image>
      <image>
        <url>url2</url>
        <id>id2</id>
        <source_url>source_url2</source_url>
      </image>
    </pics>
  </data>
</response>"""


def test_cat_api_service_available():
    requests_performer = cat_api.cat_api_request_performer.CatApiRequestPerformer(api.CatApi.BASE_URL)
    assert requests_performer.get_images(count=1) is not None


def test_CatApiParser_gets_images_from_xml_source(xml_source):
    images = cat_api.cat_api_xml_parser.CatApiXmlParser(xml_source).parse_images()
    assert images == [cat_api.image.Image(id='id1', url='url1', source_url='source_url1'),
                      cat_api.image.Image(id='id2', url='url2', source_url='source_url2')]


def test_CatApiParser_raises_on_incorrect_xml(bad_xml_source):
    with pytest.raises(Exception):
        cat_api.cat_api_xml_parser.CatApiXmlParser(bad_xml_source).parse_images()

