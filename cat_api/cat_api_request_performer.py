from typing import Iterable

import requests

from cat_api.exceptions import AggregateException


class CatApiRequestPerformer:
    def __init__(self, base_url: str):
        self._base_url = base_url

    IMAGES_URL = "/images/get"

    def get_images(self, count: int) -> Iterable[requests.Response]:
        max_results_per_page = 100
        request_args = {
            "format": "xml",
        }
        while count > 0:
            images_to_request = min(count, max_results_per_page)
            request_args['results_per_page'] = images_to_request
            yield self._do_request_with_retries(self.IMAGES_URL, retries=5, **request_args)
            count -= images_to_request

    def _do_request_with_retries(self, url: str, retries: int, **kwargs: dict) -> requests.Response:
        inner_exceptions = []
        for i in range(retries):
            try:
                return self._do_request(url, **kwargs)
            except Exception as e:
                inner_exceptions.append(e)
        raise AggregateException(inner_exceptions)

    def _do_request(self, url: str, **kwargs: dict) -> requests.Response:
        return requests.get(self._base_url + url, kwargs)