from typing import List


class AggregateException:
    """Contains multiple exceptions"""

    def __init__(self, inner_exceptions: List[Exception]):
        self.inner_exceptions = inner_exceptions

    def __str__(self):
        return "AggregateException: [\n{}\n]".format(self._format_exceptions())

    def _format_exceptions(self):
        return '\n'.join(["{0}: {1}".format(type(e).__name__, str(e)) for e in self.inner_exceptions])