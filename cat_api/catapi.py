from cat_api.cat_api_xml_parser import CatApiXmlParser
from cat_api.cat_api_request_performer import CatApiRequestPerformer


class CatApi:
    """Use this class to request TheCatApi"""

    BASE_URL = "http://thecatapi.com/api"

    def __init__(self):
        self._requests_performer = CatApiRequestPerformer(CatApi.BASE_URL)
        self._parser_class = CatApiXmlParser

    def get_cat_images(self, count):
        return list(self.cat_images_generator(count))

    def cat_images_generator(self, count):
        for response in self._requests_performer.get_images(count):
            parser = self._parser_class(response.text)
            yield from parser.parse_images()