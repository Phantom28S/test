from itertools import takewhile
from typing import Tuple, List

request_after_sleeptime = "Ты что не спишь?))) Бегом спать!!! Уже жесткий отбой!!!!\n"


def get_schedule(events: List[Tuple], current_time: str) -> str:
    header = u'\U0001F4CB' * 10 + "\n"
    events_before_time = _get_current_event_index(events, current_time)
    events_messages = [e[1] for e in events]

    all_events = [header] \
                 + events_messages[:events_before_time] \
                 + [_format_current_time(current_time)] \
                 + events_messages[events_before_time:]
    return ''.join(all_events)


def get_next_event(events: List[Tuple], current_time: str) -> str:
    event_index = _get_current_event_index(events, current_time)
    if event_index >= len(events):
        return _format_current_time(current_time) + request_after_sleeptime
    else:
        return events[event_index][1]


def _get_current_event_index(events: List[Tuple], current_time: str) -> int:
    return len(list(takewhile(lambda x: x[0] < current_time, events)))


def _format_current_time(current_time: str, len_segment: int=10) -> str:
    separator = "-" * len_segment
    time_layout = " Текущее время - " + current_time + " "
    return helpers.make_bold(separator + time_layout + separator) + '\n'
