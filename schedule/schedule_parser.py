from typing import List, Tuple, Iterable


def get_events_from_file(path: str) -> List[Tuple[str, str]]:
    with open(path, "r", encoding='utf-8') as file:
        return list(_parce_schedule_file(file))


def get_hours(line: str) -> str:
    return line[1:3]


def get_minutes(line: str) -> str:
    return line[4:6]


def _parce_schedule_file(file) -> Iterable[Tuple]:
    for line in file:
        if not line:
            continue
        time = _get_time_from_line(line)
        yield (time, line)


def _get_time_from_line(line: str) -> str:
    return line[1:6]