import uuid

import pytest

from persistent_storage.file_storage import JsonFileStorage


@pytest.fixture(scope="module")
def storage(storage_path):
    return JsonFileStorage(storage_path)


@pytest.fixture
def file_name():
    return str(uuid.uuid4())


def test_write_read_ok(storage: JsonFileStorage, file_name: str):
    data = {
        "Name": "Alex",
        "Age": 23,
        "Friends": [
            "Kirill",
            "Anton",
            "Alexey"
        ]
    }
    storage.write(file_name, data)
    assert storage.read(file_name) == data


def test_rewrite_file_then_read_ok(storage: JsonFileStorage, file_name):
    data = {
        "Name": "Alex",
        "Age": 23,
        "Friends": [
            "Kirill",
            "Anton",
            "Alexey"
        ]
    }
    storage.write(file_name, data)

    data["Friends"].clear()
    storage.write(file_name, data)
    assert storage.read(file_name) == data


def test_read_unexisting_file(storage: JsonFileStorage, file_name: str):
    with pytest.raises(FileNotFoundError):
        storage.read(file_name)
