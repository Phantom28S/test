import shutil

import os

import pytest

@pytest.fixture(scope="module")
def storage_path():
    return os.path.dirname(os.path.abspath(__file__)) + "\\storage\\"

@pytest.fixture(scope="module", autouse=True)
def set_environment(storage_path):
    os.mkdir(storage_path)
    yield
    shutil.rmtree(storage_path)