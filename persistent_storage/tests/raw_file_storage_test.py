import random
import string
import uuid

import pytest

from persistent_storage.file_storage import RawFileStorage


@pytest.fixture(scope="module")
def storage(storage_path):
    return RawFileStorage(storage_path)


@pytest.fixture
def file_name():
    return str(uuid.uuid4())


@pytest.fixture
def file_content():
    return ''.join([random.choice(string.printable) for i in range(500)])


def test_write_read_ok(storage: RawFileStorage, file_name: str, file_content: str):
    storage.write(file_name, file_content)
    assert storage.read(file_name) == file_content


def test_rewrite_file_then_read_ok(storage: RawFileStorage, file_name):
    storage.write(file_name, file_content())

    file_content_2 = file_content()
    storage.write(file_name, file_content_2)
    assert storage.read(file_name) == file_content_2


def test_read_unexisting_file(storage: RawFileStorage, file_name: str):
    with pytest.raises(FileNotFoundError):
        storage.read(file_name)


def test_delete_existing_user(storage: RawFileStorage, file_name: str, file_content: str):
    storage.write(file_name, file_content)
    storage.delete(file_name)
    with pytest.raises(FileNotFoundError):
        storage.read(file_name)