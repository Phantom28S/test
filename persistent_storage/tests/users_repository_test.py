from random import randint

import pytest

from persistent_storage.users_repository import UsersRepository
from states import State


@pytest.fixture(scope="module")
def storage(storage_path) -> UsersRepository:
    return UsersRepository(storage_path)


@pytest.fixture
def user_id() -> int:
    return randint(0, 20000)


def test_write_read_ok(storage: UsersRepository, user_id: int):
    storage.save(user_id, State.MAIN)
    assert storage.read(user_id) == State.MAIN


def test_exists_ok(storage: UsersRepository, user_id: int):
    storage.save(user_id, State.MAIN)
    assert storage.exists(user_id)


def test_not_exists_ok(storage: UsersRepository, user_id: int):
    assert not storage.exists(user_id)


def test_read_raises_when_no_such_user(storage: UsersRepository, user_id: int):
    with pytest.raises(KeyError):
        storage.read(user_id)