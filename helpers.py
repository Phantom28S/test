from datetime import datetime

from states import State


def make_bold(s):
    return "*" + s + "*"


def current_time_less_than_given_time(time):
    return get_current_time() <= time


def get_current_time():
    curtime = datetime.now()
    return datetime.strftime(curtime, "%H:%M")

def is_concrete_tournament(state: State) -> bool:
    return state in [State.SOCCER, State.HAT, State.POKER]

def is_concrete_pizza(state: State) -> bool:
    return state in [State.PIZZA1, State.PIZZA2, State.PIZZA3, State.PIZZA4, State.PIZZA5]