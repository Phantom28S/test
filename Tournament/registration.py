from typing import Union

from Tournament.tournaments import TournamentInfo
from persistent_storage.tournaments_repository import TournamentData


def _get_total_lines(info: TournamentInfo) -> int:
    return info.has_team_name + info.team_size + (info.check_additional_info is not None)

def try_parse_tournament_data(text:str, info: TournamentInfo) -> Union[TournamentData, None]:
    lines = text.split('\n')

    if len(lines) != _get_total_lines(info):
        return None

    team_name = None
    if info.has_team_name:
        team_name = lines[0]
        lines = lines[1:]

    additional_text = None
    if info.check_additional_info is not None:
        if not info.check_additional_info(lines[-1]):
            return None
        additional_text = lines[-1]
        lines = lines[:-1]

    if not all([len(line.split()) == 2 for line in lines]):
        return None

    return TournamentData(team_name, lines, additional_text)