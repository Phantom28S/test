from typing import Tuple, List, Union

from telebot import TeleBot, types
from telebot.apihelper import ApiException
from telebot.types import Message

import helpers
import keyboard_helper
from cat_api.catapi import CatApi
import schedule.schedule as schedule
from Tournament.tournaments import ButtonType, descriptions, tournament_transitions, tournament_infos
from Tournament import registration
from persistent_storage.tournaments_repository import TournamentsRepository
from states import State


class CommandHandler:
    COMMANDS = [  # list of tuples (command/description)
        ('/start', 'Начать общение с ботом'),
        ('/help', 'Выдает список доступных команд'),
    ]

    _path_to_schedule_folder = "schedule/"  # Нужно будет возможно потом поменять потом чуть-чуть (???)
    _name_of_schedule_file = "schedule.txt"

    def __init__(self, bot: TeleBot, cat_api: CatApi, events: List[Tuple[str, str]], tournaments: TournamentsRepository):
        self.bot = bot
        self.cat_api = cat_api
        self.events = events
        self.tournaments = tournaments

    @staticmethod
    def get_list_of_tournaments():
        text = '*Турниры:*\n *1.*' + str(ButtonType.Soccer) + ' \n *2.*' + str(ButtonType.Hat) + ' \n *3.*' + \
               str(ButtonType.Poker)
        return text

    @staticmethod
    def get_list_of_pizzas():
        text = 'Какую заказываем пиццу?'
        return text

    @staticmethod
    def get_list_of_pizzas1():
        text = 'Это то, что надо?'
        return text

    def start(self, message: Message):
        self.bot.send_message(message.chat.id, "Hello! This is SICamp bot!")

    def help(self, message: Message):
        self.bot.send_message(message.chat.id, CommandHandler._get_help_text())

    def send_cats(self, message: Message):
        try:
            images_count = CommandHandler._parse_cats_args(message.text)
        except (IndexError, ValueError):
            self.bot.send_message(message.chat.id, "Usage: /cats [1-10]")
        else:
            self._send_images(message.chat.id, self.cat_api.get_cat_images(min(10, images_count)))

    def get_schedule(self, message: Message):
        current_time = helpers.get_current_time()
        self.bot.send_message(message.chat.id, schedule.get_schedule(self.events, current_time), parse_mode="Markdown")

    def get_next_event(self, message: Message):
        current_time = helpers.get_current_time()
        self.bot.send_message(message.chat.id, schedule.get_next_event(self.events, current_time), parse_mode="Markdown")

    def _send_images(self, chat_id: str, images):
        for image in images:
            if image.url.endswith(".gif"):
                method = self.bot.send_document
            else:
                method = self.bot.send_photo
            self._send_image(method, chat_id, image.url)

    def _send_image(self, method, chat_id, url):
        try:
            method(chat_id, url)
        except ApiException:
            self.bot.send_message(chat_id, "Failed to send image: " + url)

    @staticmethod
    def _parse_cats_args(message_text):
        args = message_text.split()
        return int(args[1])

    @staticmethod
    def _get_help_text():
        return '\n'.join(["{0}: {1}".format(key, value) for (key, value) in CommandHandler.COMMANDS])

    def keyboard_in_tournaments(self, message: Message):
        keyboard = keyboard_helper.create_tournaments_keyboard()
        tournament_list = self.get_list_of_tournaments()
        self._send_keyboard(message.chat.id, tournament_list, keyboard)

    def keyboard_in_menu_pizza(self, message: Message):
        keyboard = keyboard_helper.create_pizza_menu_keyboard()
        pizza_list = self.get_list_of_pizzas()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)

    def keyboard_in_pizza(self, message: Message):
        keyboard = keyboard_helper.create_pizza_keyboard()
        pizza_list = self.get_list_of_pizzas()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)

    def keyboard_in_pizza1(self, message: Message):
        keyboard = keyboard_helper.create_pizza1_keyboard()
        pizza_list = self.get_list_of_pizzas1()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)
        self.bot.send_photo(message.chat.id, 'https://i.ytimg.com/vi/3LZR_gmfii4/maxresdefault.jpg', 'Фирменный томатный соус "Classic", сыр моцарелла, пепперони,'
                                ' ароматная свинина, ветчина, шампиньоны, сладкий зеленый перец, лук, черные оливки.')

    def keyboard_in_pizza2(self, message: Message):
        keyboard = keyboard_helper.create_pizza1_keyboard()
        pizza_list = self.get_list_of_pizzas1()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)
        self.bot.send_photo(message.chat.id, 'https://ru.toluna.com/dpolls_images/2016/06/09/8247c03d-b61d-4b14-be94-4ec3f12f31e2.jpg',
                            'Фирменный томатный соус "Classic", сыр моцарелла.')

    def keyboard_in_pizza3(self, message: Message):
        keyboard = keyboard_helper.create_pizza1_keyboard()
        pizza_list = self.get_list_of_pizzas1()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)
        self.bot.send_photo(message.chat.id, 'http://99px.ru/sstorage/56/2017/06/image_562306171455245357323.jpg',
                            'Фирменный томатный соус "Classic", пепперони, ароматная свинина, смесь 2-х сыров, смесь 3-х сыров, моцарелла, итальянские травы.')

    def keyboard_in_pizza4(self, message: Message):
        keyboard = keyboard_helper.create_pizza1_keyboard()
        pizza_list = self.get_list_of_pizzas1()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)
        self.bot.send_photo(message.chat.id, 'http://qulady.ru/images/qulady/2016/09/1431371741.jpg',
                            'Фирменный томатный соус "Classic", ветчина, ананасы. сыр моцарелла.')

    def keyboard_in_pizza5(self, message: Message):
        keyboard = keyboard_helper.create_pizza1_keyboard()
        pizza_list = self.get_list_of_pizzas1()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)
        self.bot.send_photo(message.chat.id, 'http://www.pizza-ametro.com/uploads/1/0/7/9/107909039/1-pizza.jpg',
                            'Фирменный томатный соус "Classic", пепперони, ветчина, бекон, ароматная свинина, говядина, сыр моцарелла.')

    def keyboard_in_bag(self, message: Message):
        keyboard = keyboard_helper.create_bag()
        f = open(str(message.chat.id) + '.txt', 'r')
        pizza_list = f.read()
        self._send_keyboard(message.chat.id, pizza_list, keyboard)

    def keyboard_in_order(self, message: Message):
        keyboard = keyboard_helper.create_order_keyboard()
        pizza_list = "Сам или доставить?"
        self._send_keyboard(message.chat.id, pizza_list, keyboard)

    def keyboard_in_menu(self, message: Message):
        keyboard = keyboard_helper.create_menu_keyboard()
        menu_list = '''Привет! Я Пицца-Бот.
Я помогу заказать еду в ресторане «Classic» 
и оформить доставку.
Введи команду /help и узнай все мои возможности!'''
        self._send_keyboard(message.chat.id, menu_list, keyboard)

    def _send_hide_keyboard(self, message: Message):
        self.bot.send_message(message.chat.id, CommandHandler._get_help_text(), reply_markup=types.ReplyKeyboardRemove())

    def _send_keyboard(self, chat_id: str, text:str, keyboard: types.ReplyKeyboardMarkup):
        self.bot.send_message(chat_id, text, reply_markup=keyboard, parse_mode="Markdown")

    @staticmethod
    def _try_parse_button(text: str) -> Union[State, None]:
        try:
            return ButtonType.from_string(text)
        except ValueError:
            return None

    def _handle_tournaments(self, message: Message) -> Union[State, None]:
        button_type = CommandHandler._try_parse_button(message.text)
        if button_type is None:
            return None
        next_state = tournament_transitions[button_type]
        if button_type == ButtonType.Back:
            self.keyboard_in_menu(message)
            return State.MAIN
        else:
            keyboard = keyboard_helper.create_back_keyboard()
            self._send_keyboard(message.chat.id, descriptions[next_state], keyboard)
        return next_state

    def _handle_pizza(self, message: Message) -> Union[State, None]:
        button_type = CommandHandler._try_parse_button(message.text)
        if button_type is None:
            return None
        if button_type == ButtonType.Back:
            self.keyboard_in_menu(message)
            return State.MAIN

    def _handle_pizza1(self, message: Message) -> Union[State, None]:
        button_type = CommandHandler._try_parse_button(message.text)

        if button_type == ButtonType.Pizza_info_no:
            self.keyboard_in_pizza(message)
            return State.PIZZA

        if button_type == ButtonType.Pizza_menu_info_no:
            self.keyboard_in_pizza(message)
            return State.PIZZA

        if button_type == ButtonType.Pizza_bag:
            self.keyboard_in_bag(message)
            return State.PIZZA

        if button_type == ButtonType.Pizza_order:
            self.keyboard_in_order(message)
            return State.ORDER

        if button_type == ButtonType.Pizza_back_menu:
            self.keyboard_in_menu(message)
            return State.MAIN

        if button_type == ButtonType.Delete_Bag:
            self.bot.send_message(message.chat.id, "Корзина очищена")
            file = open(str(message.chat.id) + '.txt', "w")
            file.write("Ваша корзина:" + '\n')
            file.close()
            self.keyboard_in_menu(message)
            return State.MAIN

        if button_type == ButtonType.Order_restaurant:
            keyboard = types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
            button_back = types.KeyboardButton(text="🍕 Вернуться в меню")
            button_phone = types.KeyboardButton(text="📱 Отправить номер телефона", request_contact=True)
            button_geo = types.KeyboardButton(text="🌍 Отправить местоположение", request_location=True)
            keyboard.add(button_back, button_phone, button_geo)
            self.bot.send_message(message.chat.id, "Отправьте свой номер телефона и координаты для заказа", reply_markup=keyboard)
            return State.ORDER2

        if button_type == ButtonType.Pizza_info_yes:
            keyboard = types.InlineKeyboardMarkup()
            callback_button = types.InlineKeyboardButton(text="Добавить за 2199 тнг.", callback_data="test1")
            keyboard.add(callback_button)

            self.bot.send_message(message.chat.id, "Большая (40 см)", reply_markup=keyboard)
            keyboard = types.InlineKeyboardMarkup()
            callback_button = types.InlineKeyboardButton(text="Добавить за 1899 тнг.", callback_data="test2")
            keyboard.add(callback_button)

            self.bot.send_message(message.chat.id, "Средняя (30 см)", reply_markup=keyboard)
            keyboard = types.InlineKeyboardMarkup()
            callback_button = types.InlineKeyboardButton(text="Добавить за 1599 тнг.", callback_data="test3")
            keyboard.add(callback_button)

            self.bot.send_message(message.chat.id, "Маленькая (20 см)", reply_markup=keyboard)
            self.keyboard_in_menu_pizza(message)
            return

    @staticmethod
    def _get_total_lines(info):
        return info.has_team_name + info.team_size + (info.check_additional_info is not None)

    def handle_default(self, message: Message, state: State) -> Union[State, None]:
        if state == State.TOURNAMENTS:
            return self._handle_tournaments(message)
        if state == State.PIZZA:
            return self._handle_pizza(message)
        if state == State.PIZZA1:
            return self._handle_pizza1(message)
        if state == State.PIZZA2:
            return self._handle_pizza1(message)
        if state == State.PIZZA3:
            return self._handle_pizza1(message)
        if state == State.PIZZA4:
            return self._handle_pizza1(message)
        if state == State.PIZZA5:
            return self._handle_pizza1(message)
        if state == State.BAG:
            return self._handle_pizza1(message)
        if state == State.ORDER:
            return self._handle_pizza1(message)
        if state == State.ORDER2:
            return self._handle_pizza1(message)
        elif helpers.is_concrete_tournament(state):
            return self._handle_concrete_tournament(message, state)
        elif helpers.is_concrete_pizza(state):
            return self._handle_concrete_pizza(message, state)
        else:
            return None

    def _handle_concrete_tournament(self, message: Message, state: State) -> State:
        button_type = CommandHandler._try_parse_button(message.text)
        if button_type is None:
            tournament_info = tournament_infos[state]
            tournament_data = registration.try_parse_tournament_data(message.text, tournament_info)
            if tournament_data is not None:
                self.tournaments.save(message.from_user.id, state, tournament_data)
                self.bot.send_message(message.chat.id, 'Регистрация прошла успешно')
                self.keyboard_in_tournaments(message)
                return State.TOURNAMENTS
            else:
                self.bot.send_message(message.chat.id, 'Форма заполнена некорректно, попробуй еще раз')
                self.bot.send_message(message.chat.id, descriptions[state], parse_mode="Markdown")
                return state
        else:
            if button_type == ButtonType.Back:
                self.keyboard_in_tournaments(message)
                return State.TOURNAMENTS

    def _handle_concrete_pizza(self, message: Message, state: State) -> State:
        button_type = CommandHandler._try_parse_button(message.text)
        if button_type is None:
                if state != State.BAG:
                    self.bot.send_message(message.chat.id, 'Выбирай пиццу2')
                    self.keyboard_in_tournaments(message)
                    return state
        else:
            if button_type == ButtonType.Back:
                self.keyboard_in_tournaments(message)
                return State.PIZZA
